import LocalStrategy from 'passport-local-roles';
import generateHttpResponse from '../api/handlers/http-response-handler';
import User from '../api/models/User';
import config from '../config';

export default (passport) => {
    passport.serializeUser((user, done) => {
		    done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
      User.findById(id)
      .then(user => {
        done(null, user);
      })
      .catch(error => {
        done(error, null);
      });
    });

    passport.use('local-register', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        roleField: 'role',
        passReqToCallback : true
    },
    (req, username, password, role, done) => {
      const secretKey = req.body.secretKey;
      if (!secretKey) {
        return done(generateHttpResponse(400, 'Secret key must be supplied.'));
      }
      if (secretKey !== config.secret.registrationKey) {
        return done(generateHttpResponse(400, 'Invalid registration key supplied.'));
      }
      if (!User.isValidRole(role)) {
        return done(generateHttpResponse(400, `Invalid role supplied. Must be either 'admin', 'upload', or 'user'`));
      }
      if (req.user) {
        return done(generateHttpResponse(400, `You can't register a new account if your logged in.`));
      }

      User.findByUsername(username)
      .then(user => {
        if (user) {
          return done(generateHttpResponse(400, 'Username taken'));
        }
        User.insertUser(username, password, role)
        .then(user => done(null, user))
        .catch(error => done(generateHttpResponse(500, error.message)));
      })
      .catch(error => done(generateHttpResponse(500, error.message)));
    }));

    passport.use('local-login', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        roleField: 'role',
        passReqToCallback : true
    },
    (req, username, password, role, done) => {
      if (req.user) {
        return done(generateHttpResponse(400, 'Already logged in'));
      }
      User.findByUsername(username)
      .then(user => {
        if (!user) {
          return done(generateHttpResponse(404, 'User not found'));
        }
        if (!User.checkPassword(password, user.password)) {
          return done(generateHttpResponse(401, 'Invalid credentials'));
        }
        return done(null, user);
      })
      .catch(error => done(generateHttpResponse(500, error.message)));
    })
  );
};
