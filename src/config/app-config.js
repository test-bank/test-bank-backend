import config from './index';
import session from 'express-session';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import flash from 'connect-flash';

function configureApp(app, passport) {
  app.use(morgan('short'));
  app.use(cookieParser());
  app.use(bodyParser.urlencoded({
  	extended: true
  }));
  app.use(bodyParser.json());
  app.use(session({
  	secret: config.secret.sessionKey,
  	resave: true,
  	saveUninitialized: true
  }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(flash());

  app.use((req, res, next) => {
  	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
  	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  	res.setHeader('Access-Control-Allow-Credentials', true);
  	next();
  });
}

export default configureApp;
