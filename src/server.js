import express from 'express';
import http from 'http';
import configurePassport from './config/passport';
import configureApp from './config/app-config';
import routes from './api/routes';
import Logger from './api/lib/logger';
import passport from 'passport';
import multer from 'multer';

const app = express();
const server = http.Server(app);
const port = 3000;

configurePassport(passport);
configureApp(app, passport);
routes(app, passport, multer());

server.listen(port);
Logger.info('Testbank restful API started on port: ' + port);
