import testbankRoutes from './testbank';
import authRoutes from './user';
import adminRoutes from './admin';
import generateHttpResponse from '../handlers/http-response-handler';

export default (app, passport, upload) => {
  app.all('/api/*', isLoggedIn);
  app.all('/api/admin*', isAdmin);

  testbankRoutes(app, upload);
  authRoutes(app, passport);
  adminRoutes(app);

  function isLoggedIn(req, res, next) {
	  if (req.isAuthenticated()) {
      return next();
    }
    return res.status(401).json(generateHttpResponse(401, 'Unauthorized'));
  }

  function isAdmin(req, res, next) {
    if (req.user.role === 'admin') {
      return next();
    }
    return res.status(401).json(generateHttpResponse(401, 'Unauthorized'));
  }
};
