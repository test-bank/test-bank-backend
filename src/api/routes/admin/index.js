import generateHttpResponse from '../../handlers/http-response-handler';
import User from '../../models/User';

export default (app) => {
  app.get('/api/admin/users', (req, res) => {
    User.findAll()
    .then(users => {
      res.status(200).json(generateHttpResponse(200, 'Successfully retrieved users', { users }));
    })
    .catch(error => {
      res.status(500).json(generateHttpResponse(500, 'Failed to retrieve users', { error }));
    });
  });

  app.route('/api/admin/user')
    .delete((req, res) => {
      const username = req.body.username;
      if (!username) {
        res.status(400).json(generateHttpResponse(400, 'Missing field username'));
      }
      User.deleteUser(username)
      .then(user => {
        res.status(200).json(generateHttpResponse(200, 'Successfully deleted user', { user }));
      })
      .catch(error => {
        if (error === 404) {
          res.status(404).json(generateHttpResponse(404, 'No user with that username exists'));
        }
        res.status(500).json(generateHttpResponse(500, 'Failed to delete user', { error }));
      });
    })
    .put((req, res) => {
      const id = req.body.id;
      const username = req.body.username;
      const role = req.body.role;

      if (!(id && username && role)) {
        res.status(400).json(generateHttpResponse(400, 'Missing required fields'));
      }

      User.updateUser(id, username, role)
      .then(user => {
        res.status(200).json(generateHttpResponse(200, 'Successfully updated user', { user }));
      })
      .catch(error => {
        res.status(error.status).json(error);
      })
    });
};
