import TestbankController from '../../controllers/testbank-controller';
import generateHttpResponse from '../../handlers/http-response-handler';

export default (app, upload) => {
  app.get('/api/testbank', (req, res) => {
    TestbankController.getTestbank()
    .then(testbank => {
      res.status(200).json(generateHttpResponse(200, 'successfully retrieved testbank', { testbank }));
    })
    .catch(error => {
      res.status(500).send(generateHttpResponse(500, 'failed to retrieve testbank', { error }));
    });
  });

  app.post('/api/testbank/upload', upload.array('files', 10), (req, res) => {
    const role = req.user.role;
    if (role === 'user') {
      res.status(401).json(generateHttpResponse(401, 'Unauthorized'));
      return;
    }
    TestbankController.uploadFilesToCourse(req.body.course, req.files)
    .then(success => {
      res.status(200).json(generateHttpResponse(200, success.message));
    })
    .catch(error => {
      res.status(500).json(generateHttpResponse(500, error.message, error.error));
    })
  });
};
