import generateHttpResponse from '../../handlers/http-response-handler';

export default (app, passport) => {
  app.post('/login', (req, res, next) => {
    passport.authenticate('local-login', (err, user) => {
      if (err) {
        return res.status(err.status).json(err);
      }
      req.login(user, (err) => {
        if (err) {
          return next(err);
        }
        return res.status(200).json(generateHttpResponse(200, 'login successful', { user }));
      });
    })(req, res, next);
  });

  app.get('/me', (req, res) => {
    const user = req.user
    if (user) {
      return res.status(200).json(generateHttpResponse(200, 'found', { user }));
    }
    return res.status(400).json(generateHttpResponse(400, 'Not logged in'));
  })

  app.post('/register', (req, res, next) => {
    passport.authenticate('local-register', (err, user) => {
      if (err) {
        return res.status(err.status).json(err);
      }
      if (!user) {
        return res.status(500).json(generateHttpResponse(500, 'Failed to create user'));
      }
      req.login(user, (err) => {
        if (err) {
          return next(err);
        }
        return res.status(200).json(generateHttpResponse(200, 'registration successful and login successful', { user }));
      })
    })(req, res, next);
  });

  app.post('/logout', (req, res) => {
    if (!req.user) {
      return res.status(400).json(generateHttpResponse(400,'Already logged out'));
    }
    req.logout();
    res.status(200).json(generateHttpResponse(200,'logout successful'));
  });
};
