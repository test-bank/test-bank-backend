import cacheManager from 'cache-manager';
import es6Promise from 'es6-promise';

const memoryCache = cacheManager.caching({
  store: 'memory',
  ttl: 3600,
  promiseDependency: es6Promise.Promise
});

export default memoryCache;
