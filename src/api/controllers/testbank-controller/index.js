import config from '../../../config';
import Testbank from '../../models/testbank';
import memoryCache from './testbank-cache';
import AWS from 'aws-sdk';

const { accessKeyId, secretAccessKey, region } = config.aws;
const credentials = new AWS.Credentials({ accessKeyId, secretAccessKey });
AWS.config.update({ region, credentials });

function getTestbank() {
  return memoryCache.wrap('testbank', () => {
    const params = {
      Bucket: config.aws.s3.bucket
    };

    return new AWS.S3().listObjects(params)
    .promise()
    .then(data => {
      const courses = data.Contents.reduce(Testbank.reducer, []);
      return Object.keys(courses).map(key => {
        const directory = courses[key];
        const name = key.substring(0, 8);
        const size = round(Testbank.calculateTotalSize(directory), 1);
        const count = Testbank.calculateFileCount(directory);
        return { name, directory, size: `${size} mb`, count };
      });
    });
  });
}

function round(value, precision) {
  const multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
}

function uploadFilesToCourse(course, files) {
  const promises = files.map(file => {
    const key = `${course}/${file.originalname}`;
    const body = file.buffer;
    return uploadFile(key, body);
  })

  return Promise.all(promises)
  .then(() => ({ message: `Successfully uploaded ${promises.length} files to ${course}.` }))
  .catch(error => ({ message: 'Failed to upload files!', error }));
}

function uploadFile(Key, Body) {
  const params = { Bucket: config.aws.s3.bucket, Key, Body };
  const managedUpload = new AWS.S3.ManagedUpload({ params });
  return managedUpload.promise();
}

export default {
  getTestbank,
  uploadFilesToCourse
};
