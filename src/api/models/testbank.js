import config from '../../config';

const baseCategories = {
  "Powerpoints": (name) => name.match('.*ppt.?$') !== null,
  "Homework": (name) => ['homework', 'hw', 'assignment'].some((str) => name.indexOf(str) !== -1),
  "Exams": (name) => ['exam', 'final', 'midterm', 'test ', ' test', 'test'].some((str) => name.indexOf(str) !== -1),
  "Study Material": (name) => ['cheat', 'study', 'guide', 'sheet'].some((str) => name.indexOf(str) !== -1),
  "Quizzes": (name) => name.indexOf('quiz') !== -1,
  "Reports": (name) => ['lab', 'report', 'labreport'].some((str) => name.indexOf(str) !== -1),
  "Projects": (name) => name.indexOf('project') !== -1,
  "Notes": (name) => name.indexOf('note') !== -1,
  "Textbooks": (name, size) => size > 10
};

function deserialize(map) {
  const key = map.Key;
  const size = map.Size;
  const course = key.substring(0, key.indexOf('/'));
  const name = key.substring(key.lastIndexOf('/') + 1, key.length);

  return { size, course, name };
}

function getCategoryForFile(file) {
  const categories = Object.keys(baseCategories);
  for (const index in categories) {
    const category = categories[index];
    const predicate = baseCategories[category];
    if (predicate(file.name.toLowerCase(), file.size)) {
      return category;
    }
  }
  return 'Other';
}

function generateFile(file, s, course) {
  const name = file.replace(new RegExp(' ', 'g'), '+');
  const dir = course.replace(new RegExp(' ', 'g'), '+');
  const size = s/1000000;
  const href = `https://s3.us-east-2.amazonaws.com/${config.aws.s3.bucket}/${dir}/${name}`;
  return { name, size, href };
}

function reducer(accum, map) {
  if (map.Key.indexOf('.') === -1) return accum;

  const { size, course, name } = deserialize(map);
  const file = generateFile(name, size, course);
  const category = getCategoryForFile(file);

  if (!accum[course]) accum[course] = {};
  if (!accum[course][category]) accum[course][category] = [];

  const files = accum[course][category];
  files.push(file);
  accum[course][category] = files;
  return accum;
}

function calculateTotalSize(directory) {
  return Object.keys(directory).reduce((accum, key) => {
    const folder = directory[key];
    return accum + Object.keys(folder).reduce((accum, key) => accum + folder[key].size, 0);
  }, 0);
}

function calculateFileCount(directory) {
  return Object.keys(directory).reduce((accum, key) => accum + directory[key].length, 0);
}

export default {
  reducer,
  calculateTotalSize,
  calculateFileCount
}
