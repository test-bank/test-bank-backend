import bcrypt from 'bcrypt-nodejs'
import config from '../../../config';
import connection from './connection';

function isValidRole(role) {
  return config.model.validRoles.indexOf(role) !== -1;
}

function checkPassword(password, hash) {
  return bcrypt.compareSync(password, hash);
}

function findById(id) {
  return new Promise((resolve, reject) => {
    connection.query('SELECT * FROM users WHERE id = ?', [id], (err, rows) => {
      if (err) reject(err);
      resolve(rows[0]);
    });
  });
}

function findByUsername(username) {
  return new Promise((resolve, reject) => {
    connection.query('SELECT * FROM users WHERE username = ?', [username], (err, rows) => {
      if (err) reject(err);
      resolve(rows[0]);
    });
  });
}

function insertUser(username, pass, role) {
  const insertQuery = `INSERT INTO users ( username, password, role ) values (?, ?, ?)`;
  const password = bcrypt.hashSync(pass);
  return new Promise((resolve, reject) => {
    connection.query(insertQuery, [username, password, role], (err, rows) => {
      if (err) reject(err);
      const user = { id: rows.insertId, username, password, role };
      resolve(user);
    });
  });
}

function deleteUser(username) {
  return new Promise((resolve, reject) => {
    findByUsername(username)
    .then(user => {
      if (!user) {
        reject(new Error({ status: 404 }));
      }

      connection.query(`DELETE FROM users WHERE username = ?`, [username], (err) => {
        if (err) reject(err);
        resolve(user);
      });
    })
    .catch(error => reject(error));
  });
}

function findAll() {
  return new Promise((resolve, reject) => {
    connection.query('SELECT * FROM users', (err, rows) => {
      if (err) reject(err);
      resolve(rows);
    });
  });
}

function updateUser(id, username, role) {
  return new Promise((resolve, reject) => {
    if (!isValidRole(role)) {
      reject({ status: 400, message: 'Invalid role supplied' });
      return;
    }

    findById(id)
    .then(user => {
      if (!user) {
        reject({ status: 404, message: 'No user with that id was found' });
        return;
      }
      findByUsername(username)
      .then(usr => {
        if (usr) {
          reject({ status: 400, message: 'Username taken' });
          return;
        }

        const updateQuery = `UPDATE users SET username = ?, role = ? WHERE id = ?`;
        connection.query(updateQuery, [username, role, id], (error) => {
          if (error) reject({ status: 500, message: error });
          resolve(Object.assign(user, { username, role }));
        });
      })
      .catch(error => reject(error));
    })
    .catch(error => reject(error));
  });
}

export default {
  checkPassword,
  isValidRole,
  findById,
  findByUsername,
  insertUser,
  findAll,
  deleteUser,
  updateUser
}
