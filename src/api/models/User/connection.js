import mysql from 'mysql';
import config from '../../../config';

const { host, username, password } = config.aws.rds;
const connection = mysql.createConnection({
  host,
  user: username,
  password
});

connection.query('USE users');

export default connection;
