import User from '../index';
import bcrypt from 'bcrypt-nodejs';

describe('user', () => {
  describe('isValidRole', () => {
    it('should return true if the role is in the predifined roles', () => {
      expect(User.isValidRole('admin')).toBeTruthy();

      expect(User.isValidRole('upload')).toBeTruthy();

      expect(User.isValidRole('user')).toBeTruthy();
    });

    it('should return false if the role is not in the predifined roles', () => {
      expect(User.isValidRole('burp')).toBeFalsy();
    });
  });

  describe('checkPassword', () => {
    it('should return true if the password hash matches the given password', () => {
      const password = 'password';
      const hash = bcrypt.hashSync(password);

      expect(User.checkPassword(password, hash)).toBeTruthy();
    });

    it('should return false if the password hash does not match the given password', () => {
      const password = 'password';
      const hash = bcrypt.hashSync(password);

      expect(User.checkPassword('other', hash)).toBeFalsy();
    });
  })
});
