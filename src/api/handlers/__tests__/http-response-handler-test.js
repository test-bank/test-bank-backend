import generateHttpResponse from '../http-response-handler';

describe('http-response-handler', () => {
  it('should generate a json object with status and message', () => {
    const status = 500;
    const message = 'Some message';
    const httpResponse = generateHttpResponse(status, message);

    expect(httpResponse).toEqual({ status, message });
  });

  it('should add the extra fields that are supplied', () => {
    const status = 500;
    const message = 'Some message';
    const extra1 = 'yo';
    const extra2 = 'hey';
    const httpResponse = generateHttpResponse(status, message, { extra1, extra2 });

    expect(httpResponse).toEqual({ status, message, extra1, extra2 });
  });
});
