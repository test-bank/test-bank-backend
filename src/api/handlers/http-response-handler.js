function generateHttpResponse(status, message, extras = {}) {
  return Object.assign(extras, {
    status,
    message
  });
}

export default generateHttpResponse;
