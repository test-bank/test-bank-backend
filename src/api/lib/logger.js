function info(payload) {
  console.log('[INFO]', payload);
}

function debug(payload) {
  console.log('[DEBUG]', payload);
}

export default {
  info,
  debug
};
